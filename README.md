# PHP Connectors
## Project Structure
Once you have checked out this project please make sure you have your application's environment variables set. 
You easily set it up by grabbing the default ones from the ``src/.env.example`` file.

    cp src/.env.example src/.env
    
**NOTE:** By setting your environment variable **APP_IGNORE_WARNINGS** to **true** you will force the application to keep running with warnings
and notices. All those alerts should still be logged in the php-fpm logs file.

## How to run Docker

First, you need to make sure you have installed ```docker``` and ```docker-compose```. 
    
Once you have all of that, simply open a terminal and navigate to the directory where ```docker-compose.yml``` 
is located, then run:
```
docker-compose build
```
then
```
docker-compose up -d
```

**Entry Point for web login / web server:** http://localhost:666/
**Entry Point for api / producer:** http://localhost:8080/
**Entry Point for rabbit mq admin:** http://localhost:15672/


Database migrations:
CREATE TABLE `users` (`id` INT NOT NULL AUTO_INCREMENT, `username` VARCHAR(120),`password` VARCHAR(120),`email` VARCHAR,`status` TINYINT(1),PRIMARY KEY (`id`)) ENGINE=InnoDB;

INSERT INTO users ('id', 'username', 'password', 'email', 'status') VALUES ('1', 'helloprint', 'P@ssw0rd!', 'xxxxxx@xxxxxx.xxx', 1);
