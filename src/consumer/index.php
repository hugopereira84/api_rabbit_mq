<?php


##from: https://www.leaseweb.com/labs/2015/10/creating-a-simple-rest-api-in-php/
require_once __DIR__ . '/vendor/autoload.php';

use PhpAmqpLib\Connection\AMQPStreamConnection;

#
## connection to the server
$connection = new AMQPStreamConnection('rabbitmq', 5672, 'hpereira', 'hpereira');
$channel = $connection->channel();

#
## declare a queue to be used
$channel->queue_declare('distributedSystem', false, true, false, false);

echo ' [*] Waiting for messages. To exit press CTRL+C', "\n";
$callback = function($msg) {
    $data = json_decode($msg->body,true);
    $dataUser = getInfoUserFromUserName($data['username']);
    $email = $dataUser['email'];
    $password = $dataUser['password'];

    // send email
    mail($email,"Password","Your password is:".$password);

    $waitSeconds = rand(15,30);
    sleep($waitSeconds);
    echo " [x] Done", "\n";
    $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
};
$channel->basic_qos(null, 1, null);
$channel->basic_consume('distributedSystem', '', false, false, false, false, $callback);
while(count($channel->callbacks)) {
    $channel->wait();
}

function getInfoUserFromUserName($username){
    $pdo = new \PDO("sqlite:api_users.db");
    $stmt = $pdo->prepare('select email, password from users where username = :username');
    //$stmt->execute([':username' => $_POST['username']]);
    $stmt->execute([':username' => $username]);
    return $stmt->fetch(PDO::FETCH_ASSOC);
    $email = $data['email'];
    $password = $data['password'];
    echo $email."/".$password;
}